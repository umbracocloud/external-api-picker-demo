﻿using System.Collections.Generic;

namespace ExternalApiPickerDemo.Core.Demo
{
    internal class CountryApiWrapper
    {
        public CountryApiWrapper()
        {
        }

        public IEnumerable<CountryDTO> AllCountries => new List<CountryDTO>() { new CountryDTO() {CountryISOCode = "da", CountryName = "Denmark" }, new CountryDTO() { CountryISOCode = "in", CountryName = "India" } };
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Cms.Web.Common;
using Umbraco.Cms.Web.Common.Attributes;
using Umbraco.Extensions;
using Umbraco.KeyValuePropertyEditor;

namespace ExternalApiPickerDemo.Core.Demo
{
    [PluginController("Sample")]
	public class CountryDemoApiController : KeyValueUmbracoPropertyEditorController
	{
		private readonly UmbracoHelper _umbracoHelper;

		public CountryDemoApiController(UmbracoHelper umbracoHelper)
			=> _umbracoHelper = umbracoHelper;
		public override IOrderedEnumerable<KeyValuePair<string, string>> GetKeyValueList(int nodeId, string propertyAlias, int uniqueFilter = 0, int allowNull = 0)
		{
			try
			{
				string[] usedUpCountryCodes = Array.Empty<string>();
				try {
					var parent = _umbracoHelper.Content(nodeId).Parent;
					usedUpCountryCodes = (parent == null ? _umbracoHelper.Content(nodeId).Children.Where(c => c.Id != nodeId).Select(c => c.Value<string>(propertyAlias)?.ToLowerInvariant()) : parent.Children.Where(c => c.Id != nodeId).Select(c => c.Value<string>(propertyAlias)?.ToLowerInvariant()).Union(_umbracoHelper.Content(nodeId).Children.Where(c => c.Id != nodeId).Select(c => c.Value<string>(propertyAlias)?.ToLowerInvariant()))).ToArray();
				} catch { uniqueFilter = 0; }
				CountryDTO[] countryList = null;
				if (uniqueFilter == 1)
				{
					countryList = (new CountryApiWrapper()).AllCountries.Where(c => !usedUpCountryCodes.Contains(c.CountryISOCode.ToLowerInvariant())).ToArray();
				}
				else
				{
					countryList = (new CountryApiWrapper()).AllCountries.ToArray();
				}
				if (allowNull == 1)
				{
					countryList = countryList.Prepend(new CountryDTO { CountryISOCode = "", CountryName = "NONE" }).ToArray();
				}
				return countryList.ToDictionary(c => c.CountryISOCode.ToLowerInvariant(), c => c.CountryName).OrderBy(v => v.Value);
			}
			catch
			{
				return null;
			}
		}
	}
}

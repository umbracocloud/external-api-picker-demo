﻿namespace ExternalApiPickerDemo.Core.Demo
{
    public class CountryDTO
    {
        public string CountryISOCode { get; set; }
        public string CountryName { get; set; }
    }
}